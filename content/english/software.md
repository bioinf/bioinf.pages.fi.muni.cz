---
author: admin
date: 2018-11-10 00:28:38+00:00
draft: false
title: About W3 Simple
type: page
url: /software/
---

[TE-greedy-nester](https://academic.oup.com/bioinformatics/article/36/20/4991/5871348): structure-based detection of LTR retrotransposons and their nesting 


"[Noise-cancelling repeat finder](https://academic.oup.com/bioinformatics/article/35/22/4809/5530597): uncovering tandem repeats in error-prone long-read sequencing data" by Robert S Harris

[pqsfinder](https://academic.oup.com/bioinformatics/article/36/8/2584/5674042) web: G-quadruplex prediction using optimized pqsfinder algorithm
