---
author: admin
date: 2018-11-10 00:28:38+00:00
draft: false
title: Bioinformatics Group FI MU
type: page
url: /about/
---

  
  
We are particularly interested in biological sequence analysis, with current emphasis on non-B DNA and repetitive sequences, long read sequencing and related novel algorithmic and solutions and their practical implementation to analyze biological data.