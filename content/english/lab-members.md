---
author: admin
date: 2018-11-10 00:28:38+00:00
draft: false
title: About W3 Simple
type: page
url: /lab-members/
---

<h3>Current lab members:</h3>
<ul>
<li>Matej <a href="https://is.muni.cz/auth/osoba/lexa">Lexa</a>, assistant professor</li>
<li>Monika <a href="https://is.muni.cz/auth/osoba/256590">Cechova</a>, postdoc</li>
<li>Jakub Horvath</li>
</ul>
