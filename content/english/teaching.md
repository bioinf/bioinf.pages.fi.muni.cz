---
author: admin
date: 2018-11-10 00:28:38+00:00
draft: false
title: About W3 Simple
type: page
url: /teaching/
---

<h3>Spring 2022</h3>
<ul>
<li>FI:IV106 Bioinformatics seminar</li>
<li>FI:IV121 Computer science applications in biology</li>
<li>FI:PB051 Computational methods in Bioinformatics and Systems Biology</li>
<li>FI:PV269 Advanced methods in bioinformatics</li>
</ul>

<h3>Fall 2021</h3>
<ul>
<li>FI:DFOME Formal Methods in Theory and Practice</li>
<li>FI:IV105 Bionformatics seminar</li>
<li>FI:IV107 Bioinformatics I</li>
<li>FI:IV108 Bionformatics II</li>
<li>FI:IV110 Bionformatics project I</li>
<li>FI:IV114 Bioinformatics and Systems Biology Project</li>
<li>FI:PA055 Visualizing Complex Data</li>
</ul>












