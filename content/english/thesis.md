---
author: admin
date: 2018-11-10 00:28:38+00:00
draft: false
title: About W3 Simple
type: page
url: /thesis/
---

We welcome new students who wish to prepare their Bachelor/Master thesis in collaboration with us. The research in our group focuses on the algorithmic approaches to the analysis of the repetitive DNA in both human and plant genomes. 

Suggested topics can be found in the [IS](https://is.muni.cz/auth/rozpis/), however additional topics can be negotiated without a hassle

<h3>Some of the recent student projects include:</h3>
<ul>
<li><strong>Program pre grafické dopytovanie nad anotáciami genómov</strong></li>
Bc. Peter Bolha, bakalářská práce, 28. 6. 2021
<li><strong>A computational pipeline for integrative annotation of repetitive sequences in eukaryotic genomes</strong></li>
Mgr. Jana Sitárová, diplomová práce, 22. 6. 2021
<li><strong>Vyhledávání sekvenčních vzorů v okolí sekvencí DNA podporujících tvorbu G-kvadruplexů</strong></li>
Bc. Timotej Šujan, bakalářská práce, 22. 9. 2020
<li><strong>Pipeline for analysis and visualization of double-strand breaks reparation</strong></li>
Bc. Eva Klimentová, bakalářská práce, 21. 9. 2020
<li><strong>Automatizovaná parametrizácia programu TE-greedy-nester pre vyhľadávanie transpozónov v genómových sekvenciách</strong></li>
Bc. Jakub Horváth, bakalářská práce, 26. 6. 2020
<li><strong>Optimalizace programu TE-nester pro vyhledávání fragmentovaných transpozonů</strong></li>
Bc. Ivan Vanát, bakalářská práce, 13. 2. 2020
<li><strong>Výpočtové prostredie pre anotáciu DNA sekvencií transpozónov a ich prezentáciu</strong></li>
Mgr. Michal Červeňanský, bakalářská práce, 24. 6. 2019
<li><strong>Bioinformatické zpracování sekvenačních dat pro účely genetických studií</strong></li>
Mgr. Peter Javorka, diplomová práce, 21. 6. 2018
<li><strong>Segmentace DNA sekvencí ze sekvenátoru dle repetitivnosti a její vliv na další analýzy</strong></li>
Bc. Jana Applová, bakalářská práce, 13. 2. 2018
<li><strong>Komprese biologických sekvencí pomocí sufixových stromů nebo polí</strong></li>
Mgr. Lucie Zukalová, diplomová práce, 5. 2. 2018
</ul>
