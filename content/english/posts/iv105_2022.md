---
title: "IV105 Bionformatics seminar - Fall 2022"
date: 2022-08-30T18:45:11+02:00
type: post
categories:
- Teaching
tags:
- talk
- seminar
- interdisciplinarity
draft: false
thumbnail: https://gitlab.fi.muni.cz/bioinf/bioinf.pages.fi.muni.cz/-/raw/master/images/A319.png

---

The theme of this semester is “Bioinformatics for protein structure and function” and “Reproducibility and scalability in bioinformatics”. Please join us for the talks below, either in person (room A319, FI MU) or using the MSTeams platform. Contact lexa @ fi.muni.cz for additional information.

#### 14.9. 4 PM
FI MU  
Introduction to the seminar  

#### 21.9. 4 PM
Invited Talk  
Marta Byrska Bishop  
NY Genome Center, New York, USA  
**Tentative title: High coverage whole genome sequencing of the expanded 1000 Genomes Project cohort including 602 trios**  

#### 5.10. 4 PM
Invited Talk  
Sneha Goenka  
Department of Electrical Engineering, Stanford University, USA  
**Accelerated identification of disease-causing variants with ultra-rapid nanopore genome sequencing**  

#### 12.10. 4 PM
Invited Talk  
David Safranek  
Faculty of Informatics, Masaryk University, Brno, Czech Republic  
**Reengineering Biological Systems through Boolean Networks**  

#### 19.10. 4 PM
Invited Talk  
Terezia Slaninakova  
Faculty of Informatics, Masaryk University, Brno, Czech Republic  
**Learned Indexing in Proteins: Substituting Complex Distance Calculations with Embedding and Clustering Techniques**  

#### 26.10. 4 PM
Invited Talk  
TBD  
Institution  
**TBD**  

#### 2.11. 4 PM
Invited Talk  
TBD  
Institution  
**TBD**  

#### 9.11. 4 PM
FI MU  
**Student presentations**  

#### 16.11. 4 PM
FI MU  
**Student presentations**  

#### 23.11. 4 PM
Invited Talk  
TBD  
Institution  
**TBD**  

#### 30.11. 4 PM
FI MU  
**Student presentations**  

#### 7.12. 4 PM
Invited Talk  
TBD  
Institution  
**TBD**  

