---
title: "Living in the interdisciplinary valley between algorithms and life"
date: 2021-08-18T11:30:11+02:00
type: post
categories:
- Outreach
tags:
- talk
- interdisciplinarity
draft: false
thumbnail: https://pbs.twimg.com/media/E9D9kWeWQAIre0E?format=png&name=900x900

---

Monika Cechova will speak today at 2 pm CEST at the panel discussion on “Interdisciplinary Research-How to succeed on the highway”. The event is organized by [Norbis](https://norbis.w.uib.no/panel-discussion-on-interdisciplinary-research-how-to-succeed-on-the-highway/), the Norwegian graduate research school in bioinformatics, biostatistics and systems biology. The abstract is provided below:

Today’s life-sciences are data-driven and characterized by so-called ‘big data’. Interdisciplinarity is becoming a necessity, and the field of bioinformatics requires intensive training in biology, computer science, and statistics. What is it like to be an interdisciplinary scientist? How to talk to experimentalists? Can one truly become an expert in two (much less more) fields? What are the common advantages and pitfalls to being a bioinformatician? The talk and the discussion will be based on the article 
Ten simple rules for biologists initiating a collaboration with computer scientists (https://doi.org/10.1371/journal.pcbi.1008281)
by Monika Cechova

*title inspired by the interview between Pavel Houser and Fatima Cvrčková: “Bioinformatika – na půl cestě mezi algoritmy a životem”
